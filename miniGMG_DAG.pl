eval 'exec perl $0 $*'
        if 0;
$NumArgs = $#ARGV+1;

$ARGV0 = $ARGV[0];
$ARGV0 =~ tr/A-Z/a-z/;

#==========================================================================================================================================
 $Boxes= 1;$BoxDim=32;$Ghosts=0;$BoxDimWithGhosts=$BoxDim+$Ghosts+$Ghosts;
#$Boxes=64;$BoxDim=64;$Ghosts=0;$BoxDimWithGhosts=$BoxDim+$Ghosts+$Ghosts; # SC'12 size problem
#==========================================================================================================================================
#$Task_Dim_i=$BoxDimWithGhosts;$Task_Dim_j=$BoxDimWithGhosts;$Task_Dim_k=$BoxDimWithGhosts; # no blocking
 $Task_Dim_i=$BoxDimWithGhosts;$Task_Dim_j=$BoxDimWithGhosts;$Task_Dim_k=                1; # planes = easy wavefront
#$Task_Dim_i=$BoxDimWithGhosts;$Task_Dim_j=$BoxDimWithGhosts;$Task_Dim_k=$BoxDimWithGhosts; # no blocking
#$Task_Dim_i=$BoxDimWithGhosts;$Task_Dim_j=$BoxDimWithGhosts;$Task_Dim_k=                1; # planes = easy wavefront
#$Task_Dim_i=$BoxDimWithGhosts;$Task_Dim_j=                8;$Task_Dim_k=                1; # SC'12 style with >4KB of spatial locality
#$Task_Dim_i=$BoxDimWithGhosts;$Task_Dim_j=                8;$Task_Dim_k=                8; # omp task style
#$Task_Dim_i=                4;$Task_Dim_j=                4;$Task_Dim_k=                4; # future brick style
#==========================================================================================================================================
if($Task_Dim_i%2 != 0){printf("Task_Dim's must be event\n");exit(0);}
if($Task_Dim_j%2 != 0){printf("Task_Dim's must be event\n");exit(0);}
#if($Task_Dim_k%2 != 0){printf("Task_Dim's must be event\n");exit(0);}
#==========================================================================================================================================
$smooths = 4;
for($s=0;$s<$smooths+1;$s++){
for($b=0;$b<$Boxes;$b++){
for($k=0;$k<$BoxDimWithGhosts;$k+=$Task_Dim_k){
for($j=0;$j<$BoxDimWithGhosts;$j+=$Task_Dim_j){
for($i=0;$i<$BoxDimWithGhosts;$i+=$Task_Dim_i){
  $DoTask = 1;
  # FIX - verify this tasks range of updates are needed... 
  # does the box at ($i,$j,$k) of size ($Task_Dim_i,$Task_Dim_j,$Task_Dim_k) intersect the box at ($s+1,$s+1,$s+1) of size ($BoxDimWithGhosts-$s-2)^3
  if($DoTask){
    if($s<$smooths){printf(" smooth_%01d%03d%03d%03d%03d ",$s,$b,$i,$j,$k);}
                else{printf("residual_%03d%03d%03d%03d ",$b,$i,$j,$k);}
    $tasks=0; # number of tasks this task must wait for before firing...
    # 7-point stencil...
    $ti=$i;$tj=$j;$tk=$k;            if( ($s-1>=0)&&($ti>=0)&&($ti<$BoxDimWithGhosts)&&($tj>=0)&&($tj<$BoxDimWithGhosts)&&($tk>=0)&&($tk<$BoxDimWithGhosts) ){printf("smooth_%01d%03d%03d%03d%03d ",$s-1,$b,$tj,$tj,$tk);$tasks++;}
    $ti=$i-$Task_Dim_i;$tj=$j;$tk=$k;if( ($s-1>=0)&&($ti>=0)&&($ti<$BoxDimWithGhosts)&&($tj>=0)&&($tj<$BoxDimWithGhosts)&&($tk>=0)&&($tk<$BoxDimWithGhosts) ){printf("smooth_%01d%03d%03d%03d%03d ",$s-1,$b,$tj,$tj,$tk);$tasks++;}
    $ti=$i+$Task_Dim_i;$tj=$j;$tk=$k;if( ($s-1>=0)&&($ti>=0)&&($ti<$BoxDimWithGhosts)&&($tj>=0)&&($tj<$BoxDimWithGhosts)&&($tk>=0)&&($tk<$BoxDimWithGhosts) ){printf("smooth_%01d%03d%03d%03d%03d ",$s-1,$b,$tj,$tj,$tk);$tasks++;}
    $ti=$i;$tj=$j-$Task_Dim_j;$tk=$k;if( ($s-1>=0)&&($ti>=0)&&($ti<$BoxDimWithGhosts)&&($tj>=0)&&($tj<$BoxDimWithGhosts)&&($tk>=0)&&($tk<$BoxDimWithGhosts) ){printf("smooth_%01d%03d%03d%03d%03d ",$s-1,$b,$tj,$tj,$tk);$tasks++;}
    $ti=$i;$tj=$j+$Task_Dim_j;$tk=$k;if( ($s-1>=0)&&($ti>=0)&&($ti<$BoxDimWithGhosts)&&($tj>=0)&&($tj<$BoxDimWithGhosts)&&($tk>=0)&&($tk<$BoxDimWithGhosts) ){printf("smooth_%01d%03d%03d%03d%03d ",$s-1,$b,$tj,$tj,$tk);$tasks++;}
    $ti=$i;$tj=$j;$tk=$k-$Task_Dim_k;if( ($s-1>=0)&&($ti>=0)&&($ti<$BoxDimWithGhosts)&&($tj>=0)&&($tj<$BoxDimWithGhosts)&&($tk>=0)&&($tk<$BoxDimWithGhosts) ){printf("smooth_%01d%03d%03d%03d%03d ",$s-1,$b,$tj,$tj,$tk);$tasks++;}
    $ti=$i;$tj=$j;$tk=$k+$Task_Dim_k;if( ($s-1>=0)&&($ti>=0)&&($ti<$BoxDimWithGhosts)&&($tj>=0)&&($tj<$BoxDimWithGhosts)&&($tk>=0)&&($tk<$BoxDimWithGhosts) ){printf("smooth_%01d%03d%03d%03d%03d ",$s-1,$b,$tj,$tj,$tk);$tasks++;}
    if($tasks==0){printf("root");}
    printf("\n");
  }
}}}}}
#==========================================================================================================================================
