//------------------------------------------------------------------------------------------------------------------------------
// Samuel Williams
// SWWilliams@lbl.gov
// Lawrence Berkeley National Lab
//------------------------------------------------------------------------------------------------------------------------------
#include <stdint.h>
#include "../timer.h"
//------------------------------------------------------------------------------------------------------------------------------
#define DEGREE 4

void smooth(domain_type * domain, int level, int x_id, int rhs_id, double a, double b, int sweep){
  uint64_t _timeStart = CycleTime();

  int CollaborativeThreadingBoxSize = 100000; // i.e. never
  #ifdef __COLLABORATIVE_THREADING
    CollaborativeThreadingBoxSize = 1 << __COLLABORATIVE_THREADING;
  #endif
  int omp_across_boxes = (domain->subdomains[0].levels[level].dim.i <  CollaborativeThreadingBoxSize);
  int omp_within_a_box = (domain->subdomains[0].levels[level].dim.i >= CollaborativeThreadingBoxSize);
  int box;

  if( (domain->dominant_eigenvalue_of_DinvA[level]<=0.0) && (domain->rank==0) )printf("dominant_eigenvalue_of_DinvA[%d] <= 0.0 !\n",level);
  double eigenvalue_max = 1.100*domain->dominant_eigenvalue_of_DinvA[level];
  double eigenvalue_min = 0.125*eigenvalue_max;
  double chebyshev_c = 0.5*(eigenvalue_max-eigenvalue_min);
  double chebyshev_d = 0.5*(eigenvalue_max+eigenvalue_min);
  double chebyshev_c2 = chebyshev_c*chebyshev_c; // c^2
  double chebyshev_d2 = chebyshev_d*chebyshev_d; // d^2
  double chebyshev_alpha[16];
  double  chebyshev_beta[16];
  int s;
  chebyshev_alpha[0] = 1.0/chebyshev_d;                                         
   chebyshev_beta[0] = 0.0;
  chebyshev_alpha[1] = 2.0*chebyshev_d/(2.0*chebyshev_d2-chebyshev_c2);         
   chebyshev_beta[1] =    chebyshev_c2/(2.0*chebyshev_d2-chebyshev_c2);
  for(s=2;s<16;s++){
  chebyshev_alpha[s] = 1.0/(chebyshev_d-0.25*chebyshev_alpha[s-1]*chebyshev_c2);
   chebyshev_beta[s] = chebyshev_alpha[s]*chebyshev_d-1.0;
  }


  #pragma omp parallel for private(box) if(omp_across_boxes)
  for(box=0;box<domain->subdomains_per_rank;box++){
    int i,j,k,s;
    int pencil = domain->subdomains[box].levels[level].pencil;
    int  plane = domain->subdomains[box].levels[level].plane;
    int ghosts = domain->subdomains[box].levels[level].ghosts;
    int  dim_k = domain->subdomains[box].levels[level].dim.k;
    int  dim_j = domain->subdomains[box].levels[level].dim.j;
    int  dim_i = domain->subdomains[box].levels[level].dim.i;
    double h2inv = 1.0/(domain->h[level]*domain->h[level]);
    double * __restrict__ x      = domain->subdomains[box].levels[level].grids[    x_id] + ghosts*(1+pencil+plane); // i.e. [0] = first non ghost zone point
    double * __restrict__ temp   = domain->subdomains[box].levels[level].grids[__temp  ] + ghosts*(1+pencil+plane);
    double * __restrict__ rhs    = domain->subdomains[box].levels[level].grids[  rhs_id] + ghosts*(1+pencil+plane);
    double * __restrict__ alpha  = domain->subdomains[box].levels[level].grids[__alpha ] + ghosts*(1+pencil+plane);
    double * __restrict__ beta_i = domain->subdomains[box].levels[level].grids[__beta_i] + ghosts*(1+pencil+plane);
    double * __restrict__ beta_j = domain->subdomains[box].levels[level].grids[__beta_j] + ghosts*(1+pencil+plane);
    double * __restrict__ beta_k = domain->subdomains[box].levels[level].grids[__beta_k] + ghosts*(1+pencil+plane);
    double * __restrict__ lambda = domain->subdomains[box].levels[level].grids[__lambda] + ghosts*(1+pencil+plane);

    int ghostsToOperateOn=ghosts-1;
    for(s=0;s<ghosts;s++,ghostsToOperateOn--){
      double c1 = chebyshev_alpha[(sweep+s)%DEGREE]; // limited to degree DEGREE polynomial
      double c2 = chebyshev_beta[ (sweep+s)%DEGREE]; // limited to degree DEGREE polynomial
      #pragma omp parallel for private(k,j,i) if(omp_within_a_box) collapse(2)
      for(k=0-ghostsToOperateOn;k<dim_k+ghostsToOperateOn;k++){
      for(j=0-ghostsToOperateOn;j<dim_j+ghostsToOperateOn;j++){
      for(i=0-ghostsToOperateOn;i<dim_i+ghostsToOperateOn;i++){
        int ijk = i + j*pencil + k*plane;
        double Ax =  a*alpha[ijk]*x[ijk]
                           -b*h2inv*(
                              beta_i[ijk+1     ]*( x[ijk+1     ]-x[ijk       ] )
                             -beta_i[ijk       ]*( x[ijk       ]-x[ijk-1     ] )
                             +beta_j[ijk+pencil]*( x[ijk+pencil]-x[ijk       ] )
                             -beta_j[ijk       ]*( x[ijk       ]-x[ijk-pencil] )
                             +beta_k[ijk+plane ]*( x[ijk+plane ]-x[ijk       ] )
                             -beta_k[ijk       ]*( x[ijk       ]-x[ijk-plane ] )
                            );
        temp[ijk] = c1*lambda[ijk]*(rhs[ijk]-Ax) + c2*temp[ijk]; // since we are using the eigenvalues of D^{-1}A, we need D^{-1}(b-Ax) == lambda[ijk]*(rhs[ijk]-Ax)
      }}}
      #pragma omp parallel for private(k,j,i) if(omp_within_a_box) collapse(2)
      for(k=0-ghostsToOperateOn;k<dim_k+ghostsToOperateOn;k++){
      for(j=0-ghostsToOperateOn;j<dim_j+ghostsToOperateOn;j++){
      for(i=0-ghostsToOperateOn;i<dim_i+ghostsToOperateOn;i++){
        int ijk = i + j*pencil + k*plane;
        x[ijk] = x[ijk] + temp[ijk];
      }}}
    }
  }
  domain->cycles.smooth[level] += (uint64_t)(CycleTime()-_timeStart);
}

//------------------------------------------------------------------------------------------------------------------------------
