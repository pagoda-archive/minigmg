//------------------------------------------------------------------------------------------------------------------------------
// Samuel Williams
// SWWilliams@lbl.gov
// Lawrence Berkeley National Lab
//------------------------------------------------------------------------------------------------------------------------------
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <math.h>
//------------------------------------------------------------------------------------------------------------------------------
#include <omp.h>
#ifdef __MPI
#include <mpi.h>
#endif
//------------------------------------------------------------------------------------------------------------------------------
#include "defines.h"
#include "box.h"
#include "mg.h"
#include "operators.h"
//==============================================================================


//------------------------------------------------------------------------------------------------------------------------------
void initialize_beta_to_bubble(domain_type *domain, int level, double hLevel){
  int box;
//double c1= 1.5;double c2= 0.5; // sandwiched between 1.0 and  2.0
//double c1= 5.5;double c2= 4.5; // sandwiched between 1.0 and 10.0
  double c3=10.0;
  double c1= 50.5;double c2=49.5; // sandwiched between 1.0 and 100.0
//double c3=100.0;
  for(box=0;box<domain->subdomains_per_rank;box++){
    // the center of the beta_i face is at an offset of (-0.5, 0.0, 0.0) relative to the center of the cell (0.5,0.5,0.5).  Therefore, it needs an offset of (0.0,0.5,0.5)
    memset(domain->subdomains[box].levels[level].grids[__beta_i],0,domain->subdomains[box].levels[level].volume*sizeof(double)); 
    int i,j,k;
    for(k=0;k<domain->subdomains[box].levels[level].dim.k;k++){
    for(j=0;j<domain->subdomains[box].levels[level].dim.j;j++){
    for(i=0;i<domain->subdomains[box].levels[level].dim.i;i++){
      double x = hLevel*((double)(i+domain->subdomains[box].levels[level].low.i)+0.0);
      double y = hLevel*((double)(j+domain->subdomains[box].levels[level].low.j)+0.5);
      double z = hLevel*((double)(k+domain->subdomains[box].levels[level].low.k)+0.5);
      int ijk =                                              (i+domain->subdomains[box].levels[level].ghosts)+ 
                domain->subdomains[box].levels[level].pencil*(j+domain->subdomains[box].levels[level].ghosts)+
                domain->subdomains[box].levels[level].plane *(k+domain->subdomains[box].levels[level].ghosts);
      double r = sqrt( (x-0.5)*(x-0.5) + (y-0.5)*(y-0.5) + (z-0.5)*(z-0.5) ); // center is at (x,y,z) = (0.5,0.5,0.5)
      double value = c1+c2*tanh(c3*(r-0.25));
      domain->subdomains[box].levels[level].grids[__beta_i][ijk] = value;
    }}}
  }
  for(box=0;box<domain->subdomains_per_rank;box++){
    // the center of the beta_j face is at an offset of ( 0.0,-0.5, 0.0) relative to the center of the cell (0.5,0.5,0.5).  Therefore, it needs an offset of (0.5,0.0,0.5)
    memset(domain->subdomains[box].levels[level].grids[__beta_j],0,domain->subdomains[box].levels[level].volume*sizeof(double)); 
    int i,j,k;
    for(k=0;k<domain->subdomains[box].levels[level].dim.k;k++){
    for(j=0;j<domain->subdomains[box].levels[level].dim.j;j++){
    for(i=0;i<domain->subdomains[box].levels[level].dim.i;i++){
      double x = hLevel*((double)(i+domain->subdomains[box].levels[level].low.i)+0.5);
      double y = hLevel*((double)(j+domain->subdomains[box].levels[level].low.j)+0.0);
      double z = hLevel*((double)(k+domain->subdomains[box].levels[level].low.k)+0.5);
      int ijk =                                              (i+domain->subdomains[box].levels[level].ghosts)+ 
                domain->subdomains[box].levels[level].pencil*(j+domain->subdomains[box].levels[level].ghosts)+
                domain->subdomains[box].levels[level].plane *(k+domain->subdomains[box].levels[level].ghosts);
      double r = sqrt( (x-0.5)*(x-0.5) + (y-0.5)*(y-0.5) + (z-0.5)*(z-0.5) ); // center is at (x,y,z) = (0.5,0.5,0.5)
      double value = c1+c2*tanh(c3*(r-0.25));
      domain->subdomains[box].levels[level].grids[__beta_j][ijk] = value;
    }}}
  }
  for(box=0;box<domain->subdomains_per_rank;box++){
    // the center of the beta_k face is at an offset of ( 0.0, 0.0,-0.5) relative to the center of the cell (0.5,0.5,0.5).  Therefore, it needs an offset of (0.5,0.5,0.0)
    memset(domain->subdomains[box].levels[level].grids[__beta_k],0,domain->subdomains[box].levels[level].volume*sizeof(double)); 
    int i,j,k;
    for(k=0;k<domain->subdomains[box].levels[level].dim.k;k++){
    for(j=0;j<domain->subdomains[box].levels[level].dim.j;j++){
    for(i=0;i<domain->subdomains[box].levels[level].dim.i;i++){
      double x = hLevel*((double)(i+domain->subdomains[box].levels[level].low.i)+0.5);
      double y = hLevel*((double)(j+domain->subdomains[box].levels[level].low.j)+0.5);
      double z = hLevel*((double)(k+domain->subdomains[box].levels[level].low.k)+0.0);
      int ijk =                                              (i+domain->subdomains[box].levels[level].ghosts)+ 
                domain->subdomains[box].levels[level].pencil*(j+domain->subdomains[box].levels[level].ghosts)+
                domain->subdomains[box].levels[level].plane *(k+domain->subdomains[box].levels[level].ghosts);
      double r = sqrt( (x-0.5)*(x-0.5) + (y-0.5)*(y-0.5) + (z-0.5)*(z-0.5) ); // center is at (x,y,z) = (0.5,0.5,0.5)
      double value = c1+c2*tanh(c3*(r-0.25));
      domain->subdomains[box].levels[level].grids[__beta_k][ijk] = value;
    }}}
  }
}


//------------------------------------------------------------------------------------------------------------------------------
void initialize_grid_to_sine(domain_type *domain, int level, int grid_id, double hLevel){
  double twoPi = 2.0*M_PI;
  int box;
  for(box=0;box<domain->subdomains_per_rank;box++){
    memset(domain->subdomains[box].levels[level].grids[grid_id],0,domain->subdomains[box].levels[level].volume*sizeof(double)); 
    int i,j,k;
    int n=1; // lowest frequency
    for(k=0;k<domain->subdomains[box].levels[level].dim.k;k++){
    for(j=0;j<domain->subdomains[box].levels[level].dim.j;j++){
    for(i=0;i<domain->subdomains[box].levels[level].dim.i;i++){
      double x = hLevel*((double)(i+domain->subdomains[box].levels[level].low.i)+0.5);
      double y = hLevel*((double)(j+domain->subdomains[box].levels[level].low.j)+0.5);
      double z = hLevel*((double)(k+domain->subdomains[box].levels[level].low.k)+0.5);
      int ijk =                                              (i+domain->subdomains[box].levels[level].ghosts)+ 
                domain->subdomains[box].levels[level].pencil*(j+domain->subdomains[box].levels[level].ghosts)+
                domain->subdomains[box].levels[level].plane *(k+domain->subdomains[box].levels[level].ghosts);
      double value  = sin(twoPi*n*x)*sin(twoPi*n*y)*sin(twoPi*n*z);
      domain->subdomains[box].levels[level].grids[grid_id][ijk] = value;
    }}}
  }
}


//------------------------------------------------------------------------------------------------------------------------------
void initialize_grid_to_blah(domain_type *domain, int level, int grid_id, double hLevel){
  double amplitude = 1.0;
  double scale = amplitude / 0.75;  // in 3D, maximum controbutions from x, y, and z are 0.25 each.
  int box;
  for(box=0;box<domain->subdomains_per_rank;box++){
    memset(domain->subdomains[box].levels[level].grids[grid_id],0,domain->subdomains[box].levels[level].volume*sizeof(double)); 
    int i,j,k;
    for(k=0;k<domain->subdomains[box].levels[level].dim.k;k++){
    for(j=0;j<domain->subdomains[box].levels[level].dim.j;j++){
    for(i=0;i<domain->subdomains[box].levels[level].dim.i;i++){
      double x = hLevel*((double)(i+domain->subdomains[box].levels[level].low.i)+0.5);
      double y = hLevel*((double)(j+domain->subdomains[box].levels[level].low.j)+0.5);
      double z = hLevel*((double)(k+domain->subdomains[box].levels[level].low.k)+0.5);
      int ijk =                                              (i+domain->subdomains[box].levels[level].ghosts)+ 
                domain->subdomains[box].levels[level].pencil*(j+domain->subdomains[box].levels[level].ghosts)+
                domain->subdomains[box].levels[level].plane *(k+domain->subdomains[box].levels[level].ghosts);
      // 3D pyramid...
      double value  = 0.0;
      if(x<0.5)value+=(x-0.25);else value+=(0.75-x); // i.e. -0.25...0.25...-0.25
      if(y<0.5)value+=(y-0.25);else value+=(0.75-y);
      if(z<0.5)value+=(z-0.25);else value+=(0.75-z);
      domain->subdomains[box].levels[level].grids[grid_id][ijk] = scale*value;
    }}}
  }
}


//------------------------------------------------------------------------------------------------------------------------------
int main(int argc, char **argv){
  int MPI_Rank=0;
  int MPI_Tasks=1;
  int OMP_Threads = 1;

  #pragma omp parallel 
  {
    #pragma omp master
    {
      OMP_Threads = omp_get_num_threads();
    }
  }
    

  #ifdef __MPI
  #warning Compiling for MPI...
  int MPI_threadingModel          = -1;
//int MPI_threadingModelRequested = MPI_THREAD_SINGLE;
//int MPI_threadingModelRequested = MPI_THREAD_SERIALIZED;
  int MPI_threadingModelRequested = MPI_THREAD_FUNNELED;
//int MPI_threadingModelRequested = MPI_THREAD_MULTIPLE;
      #ifdef __MPI_THREAD_MULTIPLE
      MPI_threadingModelRequested = MPI_THREAD_MULTIPLE;
      #else
  #endif
  MPI_Init_thread(&argc, &argv, MPI_threadingModelRequested, &MPI_threadingModel);
  MPI_Comm_size(MPI_COMM_WORLD, &MPI_Tasks);
  MPI_Comm_rank(MPI_COMM_WORLD, &MPI_Rank);

  if(MPI_threadingModel>MPI_threadingModelRequested)MPI_threadingModel=MPI_threadingModelRequested;
  if(MPI_Rank==0){
       if(MPI_threadingModelRequested == MPI_THREAD_MULTIPLE  )printf("Requested MPI_THREAD_MULTIPLE, ");
  else if(MPI_threadingModelRequested == MPI_THREAD_SINGLE    )printf("Requested MPI_THREAD_SINGLE, ");
  else if(MPI_threadingModelRequested == MPI_THREAD_FUNNELED  )printf("Requested MPI_THREAD_FUNNELED, ");
  else if(MPI_threadingModelRequested == MPI_THREAD_SERIALIZED)printf("Requested MPI_THREAD_SERIALIZED, ");
  else if(MPI_threadingModelRequested == MPI_THREAD_MULTIPLE  )printf("Requested MPI_THREAD_MULTIPLE, ");
  else                                                printf("got Unknown MPI_threadingModel (%d)\n",MPI_threadingModel);
       if(MPI_threadingModel == MPI_THREAD_MULTIPLE  )printf("got MPI_THREAD_MULTIPLE\n");
  else if(MPI_threadingModel == MPI_THREAD_SINGLE    )printf("got MPI_THREAD_SINGLE\n");
  else if(MPI_threadingModel == MPI_THREAD_FUNNELED  )printf("got MPI_THREAD_FUNNELED\n");
  else if(MPI_threadingModel == MPI_THREAD_SERIALIZED)printf("got MPI_THREAD_SERIALIZED\n");
  else if(MPI_threadingModel == MPI_THREAD_MULTIPLE  )printf("got MPI_THREAD_MULTIPLE\n");
  else                                                printf("got Unknown MPI_threadingModel (%d)\n",MPI_threadingModel);
  fflush(stdout);  }
    #ifdef __MPI_THREAD_MULTIPLE
    if( (MPI_threadingModelRequested == MPI_THREAD_MULTIPLE) && (MPI_threadingModel != MPI_THREAD_MULTIPLE) ){MPI_Finalize();exit(0);}
    #endif
  #endif


  int log2_subdomain_dim = 6;
  int subdomains_per_rank_in_i=256 / (1<<log2_subdomain_dim);
  int subdomains_per_rank_in_j=256 / (1<<log2_subdomain_dim);
  int subdomains_per_rank_in_k=256 / (1<<log2_subdomain_dim);
  int ranks_in_i=1;
  int ranks_in_j=1;
  int ranks_in_k=1;

  if(argc==2){
          log2_subdomain_dim=atoi(argv[1]);
          subdomains_per_rank_in_i=256 / (1<<log2_subdomain_dim);
          subdomains_per_rank_in_j=256 / (1<<log2_subdomain_dim);
          subdomains_per_rank_in_k=256 / (1<<log2_subdomain_dim);
  }else if(argc==5){
          log2_subdomain_dim=atoi(argv[1]);
    subdomains_per_rank_in_i=atoi(argv[2]);
    subdomains_per_rank_in_j=atoi(argv[3]);
    subdomains_per_rank_in_k=atoi(argv[4]);
  }else if(argc==8){
          log2_subdomain_dim=atoi(argv[1]);
    subdomains_per_rank_in_i=atoi(argv[2]);
    subdomains_per_rank_in_j=atoi(argv[3]);
    subdomains_per_rank_in_k=atoi(argv[4]);
                  ranks_in_i=atoi(argv[5]);
                  ranks_in_j=atoi(argv[6]);
                  ranks_in_k=atoi(argv[7]);
  }else if(argc!=1){
    if(MPI_Rank==0){printf("usage: ./a.out [log2_subdomain_dim]   [subdomains per rank in i,j,k]  [ranks in i,j,k]\n");}
    #ifdef __MPI
    MPI_Finalize();
    #endif
    exit(0);
  }

  if(log2_subdomain_dim>7){
    if(MPI_Rank==0){printf("error, log2_subdomain_dim(%d)>7\n",log2_subdomain_dim);}
    #ifdef __MPI
    MPI_Finalize();
    #endif
    exit(0);
  }

  if(ranks_in_i*ranks_in_j*ranks_in_k != MPI_Tasks){
    if(MPI_Rank==0){printf("error, ranks_in_i*ranks_in_j*ranks_in_k(%d*%d*%d=%d) != MPI_Tasks(%d)\n",ranks_in_i,ranks_in_j,ranks_in_k,ranks_in_i*ranks_in_j*ranks_in_k,MPI_Tasks);}
    #ifdef __MPI
    MPI_Finalize();
    #endif
    exit(0);
  }

  if(MPI_Rank==0)printf("%d MPI Tasks of %d threads\n",MPI_Tasks,OMP_Threads);

  int subdomain_dim_i=1<<log2_subdomain_dim;
  int subdomain_dim_j=1<<log2_subdomain_dim;
  int subdomain_dim_k=1<<log2_subdomain_dim;
  // fine dim = 128 64 32 16 8 4
  //   levels =   6  5  4  3 2 1
  int levels_in_vcycle=(log2_subdomain_dim+1)-2; // ie 1+log2(fine grid size)-log2(bottom grid size)

  //- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
  int box;
  domain_type domain_1 ;
  domain_type domain_CA;
  int boundary_conditions[3] = {__BOUNDARY_PERIODIC,__BOUNDARY_PERIODIC,__BOUNDARY_PERIODIC}; // i-, j-, and k-directions
  //- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
  create_domain(&domain_1 ,
                           subdomain_dim_i,subdomain_dim_j,subdomain_dim_k,
                           subdomains_per_rank_in_i,subdomains_per_rank_in_j,subdomains_per_rank_in_k,
                           ranks_in_i,ranks_in_j,ranks_in_k,
                           MPI_Rank,
                           boundary_conditions,
                           __NumGrids,1,levels_in_vcycle);
  create_domain(&domain_CA,
                           subdomain_dim_i,subdomain_dim_j,subdomain_dim_k,
                           subdomains_per_rank_in_i,subdomains_per_rank_in_j,subdomains_per_rank_in_k,
                           ranks_in_i,ranks_in_j,ranks_in_k,
                           MPI_Rank,
                           boundary_conditions,
                           __NumGrids,4,levels_in_vcycle);
  //- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
  double h0=1.0/((double)(domain_1.dim.i));
  #if 1
  //- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
  // "easy problem" = constant coefficient, RHS=product of sin's
  //- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
  if(MPI_Rank==0){printf("initializing alpha, beta, RHS for the ``easy problem''...");fflush(stdout);}
  double  a=0.9;
  double  b=0.9;
  initialize_grid_to_scalar(&domain_1 ,0,__alpha ,1.0);
  initialize_grid_to_scalar(&domain_1 ,0,__beta_i,1.0);
  initialize_grid_to_scalar(&domain_1 ,0,__beta_j,1.0);
  initialize_grid_to_scalar(&domain_1 ,0,__beta_k,1.0);
  initialize_grid_to_scalar(&domain_CA,0,__alpha ,1.0);
  initialize_grid_to_scalar(&domain_CA,0,__beta_i,1.0);
  initialize_grid_to_scalar(&domain_CA,0,__beta_j,1.0);
  initialize_grid_to_scalar(&domain_CA,0,__beta_k,1.0);
  initialize_grid_to_sine(  &domain_1 ,0,__f,h0);
  initialize_grid_to_sine(  &domain_CA,0,__f,h0);
  #else
  //- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
  // "hard problem" = beta's are 1 within r<<0.25, and 10 for r>>0.25 (uses tanh).  RHS=product of sin's
  //- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
  if(MPI_Rank==0){printf("initializing alpha, beta, RHS for the ``hard problem''...");fflush(stdout);}
  double  a=0.0;
  double  b=0.9;
  initialize_grid_to_scalar(&domain_1 ,0,__alpha,1.0);
  initialize_grid_to_scalar(&domain_CA,0,__alpha,1.0);
  initialize_beta_to_bubble(&domain_1 ,0,h0);
  initialize_beta_to_bubble(&domain_CA,0,h0);
  initialize_grid_to_blah(  &domain_1 ,0,__f,h0);
  initialize_grid_to_blah(  &domain_CA,0,__f,h0);
  #endif
  //- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
  //noise(&domain_1 ,0,__f,-1.0,1.0); // RHS = noise
  //noise(&domain_CA,0,__f,-1.0,1.0); // RHS = noise
  //noise(&domain_1 ,0,__temp,1.0,1.1);mul_grids(&domain_1 ,0,__f,1.0,__f,__temp); // add noise to __f
  //noise(&domain_CA,0,__temp,1.0,1.1);mul_grids(&domain_CA,0,__f,1.0,__f,__temp); // add noise to __f
  //noise(&domain_1 ,0,__temp,1.0,1.1);mul_grids(&domain_1 ,0,__beta_i,1.0,__beta_i,__temp); // add noise to __beta's
  //noise(&domain_1 ,0,__temp,1.0,1.1);mul_grids(&domain_1 ,0,__beta_j,1.0,__beta_j,__temp); // add noise to __beta's
  //noise(&domain_1 ,0,__temp,1.0,1.1);mul_grids(&domain_1 ,0,__beta_k,1.0,__beta_k,__temp); // add noise to __beta's
  //noise(&domain_CA,0,__temp,1.0,1.1);mul_grids(&domain_CA,0,__beta_i,1.0,__beta_i,__temp); // add noise to __beta's
  //noise(&domain_CA,0,__temp,1.0,1.1);mul_grids(&domain_CA,0,__beta_j,1.0,__beta_j,__temp); // add noise to __beta's
  //noise(&domain_CA,0,__temp,1.0,1.1);mul_grids(&domain_CA,0,__beta_k,1.0,__beta_k,__temp); // add noise to __beta's
  if(MPI_Rank==0){printf("done\n");fflush(stdout);}
  //- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
  // when using periodic boundary conditions, the RHS must sum to zero
  shift_grid_to_sum_to_zero(&domain_1 ,0,__f);
  shift_grid_to_sum_to_zero(&domain_CA,0,__f);
  //- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
  MGBuild(&domain_1 ,a,b,h0); // restrictions, dominant eigenvalue, etc...
  MGBuild(&domain_CA,a,b,h0); // restrictions, dominant eigenvalue, etc...
  //- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
  int s,sMax=2;
  #ifdef __MPI
  sMax=4;
  #endif
                    //Make initial an guess for u(=0)... Solve Lu=f to precision of 1e-10...print the benchmarking timing results...
//for(s=0;s<sMax;s++){zero_grid(&domain_1 ,0,__u);FMGSolve(&domain_1 ,__u,__f,  a,b,1e-10);}print_timing(&domain_1 );
  for(s=0;s<sMax;s++){zero_grid(&domain_1 ,0,__u); MGSolve(&domain_1 ,__u,__f,1,a,b,1e-10);}print_timing(&domain_1 );
  for(s=0;s<sMax;s++){zero_grid(&domain_CA,0,__u); MGSolve(&domain_CA,__u,__f,1,a,b,1e-10);}print_timing(&domain_CA);
  //- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
  destroy_domain(&domain_1 );
  destroy_domain(&domain_CA);
  //- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
  #ifdef __MPI
  MPI_Finalize();
  #endif
  //- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
  return(0);
}
