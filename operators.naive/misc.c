//------------------------------------------------------------------------------------------------------------------------------
// Samuel Williams
// SWWilliams@lbl.gov
// Lawrence Berkeley National Lab
//------------------------------------------------------------------------------------------------------------------------------
#include <stdint.h>
#include "../timer.h"
//------------------------------------------------------------------------------------------------------------------------------
double __box_dot(box_type *box, int id_a, int id_b){
  double a_dot_b = 0.0;
  int i,j,k;
  double * __restrict__ grid_a = box->grids[id_a];
  double * __restrict__ grid_b = box->grids[id_b];
  for(k=0;k<box->dim.k;k++){
  for(j=0;j<box->dim.j;j++){
  for(i=0;i<box->dim.i;i++){
    int ijk = (i+box->ghosts) + (j+box->ghosts)*box->pencil + (k+box->ghosts)*box->plane;
    a_dot_b += grid_a[ijk]*grid_b[ijk];
  }}}
  return(a_dot_b);
}


double __box_norm_of_grid(box_type *box, int grid_id){
  double rv = 0.0;
  int i,j,k;
  double * __restrict__ grid = box->grids[grid_id];
  for(k=0;k<box->dim.k;k++){
  for(j=0;j<box->dim.j;j++){
  for(i=0;i<box->dim.i;i++){
    int ijk = (i+box->ghosts) + (j+box->ghosts)*box->pencil + (k+box->ghosts)*box->plane;
    double fabs_grid_ijk = fabs(grid[ijk]);
    if(fabs_grid_ijk>rv){rv=fabs_grid_ijk;} // max norm
  }}}
  return(rv);
}


void __box_add_grids(box_type *box, int id_c, double scale_a, int id_a, double scale_b, int id_b){ // c=a+b
  int i,j,k;
  double * __restrict__ grid_c = box->grids[id_c];
  double * __restrict__ grid_a = box->grids[id_a];
  double * __restrict__ grid_b = box->grids[id_b];
  for(k=0;k<box->dim.k;k++){
  for(j=0;j<box->dim.j;j++){
  for(i=0;i<box->dim.i;i++){
      int ijk = (i+box->ghosts) + (j+box->ghosts)*box->pencil + (k+box->ghosts)*box->plane;
      grid_c[ijk] = scale_a*grid_a[ijk] + scale_b*grid_b[ijk];
  }}}
}


void __box_scale_grid(box_type *box, int id_c, double scale_a, int id_a){ // c=scale*a
  int i,j,k;
  double * __restrict__ grid_c = box->grids[id_c];
  double * __restrict__ grid_a = box->grids[id_a];
  for(k=0;k<box->dim.k;k++){
  for(j=0;j<box->dim.j;j++){
  for(i=0;i<box->dim.i;i++){
      int ijk = (i+box->ghosts) + (j+box->ghosts)*box->pencil + (k+box->ghosts)*box->plane;
      grid_c[ijk] = scale_a*grid_a[ijk];
  }}}
}


void __box_zero_grid(box_type *box,int grid_id){
  memset(box->grids[grid_id],0,box->volume*sizeof(double));
}

void __box_initialize_grid_to_scalar(box_type *box, int grid_id, double scalar){
  int i,j,k;
  //__box_zero_grid(box,grid_id); // ???
  for(k=0;k<box->dim.k;k++){
  for(j=0;j<box->dim.j;j++){
  for(i=0;i<box->dim.i;i++){
    double x = h*(double)(i+box->low.i);
    double y = h*(double)(j+box->low.j);
    double z = h*(double)(k+box->low.k);
    int ijk = (i+box->ghosts) + (j+box->ghosts)*box->pencil + (k+box->ghosts)*box->plane;
    double value = (scalar);
    box->grids[grid_id][ijk] = value;
  }}}
}


//===========================================================================================
void zero_grid(domain_type * domain, int level, int grid_id){
  // zero's the entire grid INCLUDING ghost zones...
  uint64_t _timeStart = CycleTime();
  int box;
  #pragma omp parallel for private(box)
  for(box=0;box<domain->subdomains_per_rank;box++){
    __box_zero_grid(&domain->subdomains[box].levels[level],grid_id);
  }
  domain->cycles.blas1[level] += (uint64_t)(CycleTime()-_timeStart);
}


void initialize_grid_to_scalar(domain_type * domain, int level, int grid_id, double scalar){
  // initializes the grid to a scalar while zero'ing the ghost zones...
  uint64_t _timeStart = CycleTime();
  int box;
  #pragma omp parallel for private(box)
  for(box=0;box<domain->subdomains_per_rank;box++){
    __box_initialize_grid_to_scalar(&domain->subdomains[box].levels[level],grid_id,h,scalar);
  }
  domain->cycles.blas1[level] += (uint64_t)(CycleTime()-_timeStart);
}


void add_grids(domain_type * domain, int level, int id_c, double scale_a, int id_a, double scale_b, int id_b){ // c=a+b
  int box;
  uint64_t _timeStart = CycleTime();
  #pragma omp parallel for private(box)
  for(box=0;box<domain->subdomains_per_rank;box++){
    __box_add_grids(&domain->subdomains[box].levels[level],id_c,scale_a,id_a,scale_b,id_b);
  }
  domain->cycles.blas1[level] += (uint64_t)(CycleTime()-_timeStart);
}


void scale_grid(domain_type * domain, int level, int id_c, double scale_a, int id_a){ // c=a+b
  int box;
  uint64_t _timeStart = CycleTime();
  #pragma omp parallel for private(box)
  for(box=0;box<domain->subdomains_per_rank;box++){
    __box_scale_grid(&domain->subdomains[box].levels[level],id_c,scale_a,id_a);
  }
  domain->cycles.blas1[level] += (uint64_t)(CycleTime()-_timeStart);
}


double norm(domain_type * domain, int level, int grid_id){
  uint64_t _timeStart = CycleTime();
  int box;
  double        max_norm =  0.0;
  #pragma omp parallel for private(box) reduction(max:max_norm)
  for(box=0;box<domain->subdomains_per_rank;box++){
    double box_norm = __box_norm_of_grid(&domain->subdomains[box].levels[level],grid_id);
    if(box_norm>max_norm){max_norm = box_norm;}
  }
  domain->cycles.blas1[level] += (uint64_t)(CycleTime()-_timeStart);

  #ifdef __MPI
  uint64_t _timeStartAllReduce = CycleTime();
  double send = max_norm;
  MPI_Allreduce(&send,&max_norm,1,MPI_DOUBLE,MPI_MAX,MPI_COMM_WORLD);
  uint64_t _timeEndAllReduce = CycleTime();
  domain->cycles.collectives[level]   += (uint64_t)(_timeEndAllReduce-_timeStartAllReduce);
  domain->cycles.communication[level] += (uint64_t)(_timeEndAllReduce-_timeStartAllReduce);
  #endif
  return(max_norm);
}


double dot(domain_type * domain, int level, int id_a, int id_b){
  uint64_t _timeStart = CycleTime();
  double        a_dot_b =  0.0;
  int box;
  #pragma omp parallel for private(box) reduction(+:a_dot_b)
  for(box=0;box<domain->subdomains_per_rank;box++){
    double a_dot_b_box = __box_dot(&domain->subdomains[box].levels[level],id_a,id_b);
    a_dot_b+=a_dot_b_box;
  }
  domain->cycles.blas1[level] += (uint64_t)(CycleTime()-_timeStart);

  #ifdef __MPI
  uint64_t _timeStartAllReduce = CycleTime();
  double send = a_dot_b;
  MPI_Allreduce(&send,&a_dot_b,1,MPI_DOUBLE,MPI_SUM,MPI_COMM_WORLD);
  uint64_t _timeEndAllReduce = CycleTime();
  domain->cycles.collectives[level]   += (uint64_t)(_timeEndAllReduce-_timeStartAllReduce);
  domain->cycles.communication[level] += (uint64_t)(_timeEndAllReduce-_timeStartAllReduce);
  #endif

  return(a_dot_b);
}
