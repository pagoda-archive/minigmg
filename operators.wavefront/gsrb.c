//------------------------------------------------------------------------------------------------------------------------------
// Samuel Williams
// SWWilliams@lbl.gov
// Lawrence Berkeley National Lab
//------------------------------------------------------------------------------------------------------------------------------
#include <stdint.h>
#include "../timer.h"
//------------------------------------------------------------------------------------------------------------------------------
void __box_smooth_GSRB_multiple_threaded(box_type *box, int phi_id, int rhs_id, double a, double b, int sweep){
  int pencil = box->pencil;
  int  plane = box->plane;
  int ghosts = box->ghosts;
  int   DimI = box->dim.i;
  int   DimJ = box->dim.j;
  int   DimK = box->dim.k;
  double h2inv = 1.0/(box->h*box->h);
  double  * __restrict__ phi          = box->grids[  phi_id] + ghosts*(1+pencil+plane);
  double  * __restrict__ rhs          = box->grids[  rhs_id] + ghosts*(1+pencil+plane);
  double  * __restrict__ alpha        = box->grids[__alpha ] + ghosts*(1+pencil+plane);
  double  * __restrict__ beta_i       = box->grids[__beta_i] + ghosts*(1+pencil+plane);
  double  * __restrict__ beta_j       = box->grids[__beta_j] + ghosts*(1+pencil+plane);
  double  * __restrict__ beta_k       = box->grids[__beta_k] + ghosts*(1+pencil+plane);
  double  * __restrict__ lambda       = box->grids[__lambda] + ghosts*(1+pencil+plane);

  #pragma omp parallel
  {
  int leadingK,planeInWavefront;
  int kLow  =     -(ghosts-1);
  int kHigh = DimK+(ghosts-1);
  for(leadingK=kLow;leadingK<kHigh;leadingK++){
    for(planeInWavefront=0;planeInWavefront<ghosts;planeInWavefront++){
    int k=(leadingK-planeInWavefront);if((k>=kLow)&&(k<kHigh)){
      int i,j;
      int ghostsToOperateOn=ghosts-1-planeInWavefront;
      #pragma omp for schedule(static,9) nowait
      #if   defined(__GSRB_CONDITIONAL)
      for(j=1-ghosts;j<DimJ+ghosts-1;j++){
      for(i=0-ghostsToOperateOn;i<DimI+ghostsToOperateOn;i++){
        int ijk = i + j*pencil + k*plane;
        int doit = ((i^j^k^planeInWavefront^sweep^1)&1);
        double helmholtz =  a*alpha[ijk]*phi[ijk]
                           -b*h2inv*(
                              beta_i[ijk+1     ]*( phi[ijk+1     ]-phi[ijk       ] )
                             -beta_i[ijk       ]*( phi[ijk       ]-phi[ijk-1     ] )
                             +beta_j[ijk+pencil]*( phi[ijk+pencil]-phi[ijk       ] )
                             -beta_j[ijk       ]*( phi[ijk       ]-phi[ijk-pencil] )
                             +beta_k[ijk+plane ]*( phi[ijk+plane ]-phi[ijk       ] )
                             -beta_k[ijk       ]*( phi[ijk       ]-phi[ijk-plane ] )
                            );
        phi[ijk] = (doit) ? phi[ijk] - lambda[ijk]*(helmholtz-rhs[ijk]) : phi[ijk];
      }}
      #elif defined(__GSRB_STRIDE2)
      for(j=1-ghosts;j<DimJ+ghosts-1;j++){
      //int iStart  = ((j^k^sweep)&1)     -ghostsToOperateOn;
      //int iEnd    = ((j^k^sweep)&1)+DimI+ghostsToOperateOn;
      //for(i=iStart;i<iEnd;i+=2){ // stride-2 GSRB
      for(i=((j^k^sweep)&1)-ghostsToOperateOn;i<DimI+ghostsToOperateOn;i+=2){ // stride-2 GSRB
        int ijk = i + j*pencil + k*plane;
        double helmholtz =  a*alpha[ijk]*phi[ijk]
                           -b*h2inv*(
                              beta_i[ijk+1     ]*( phi[ijk+1     ]-phi[ijk       ] )
                             -beta_i[ijk       ]*( phi[ijk       ]-phi[ijk-1     ] )
                             +beta_j[ijk+pencil]*( phi[ijk+pencil]-phi[ijk       ] )
                             -beta_j[ijk       ]*( phi[ijk       ]-phi[ijk-pencil] )
                             +beta_k[ijk+plane ]*( phi[ijk+plane ]-phi[ijk       ] )
                             -beta_k[ijk       ]*( phi[ijk       ]-phi[ijk-plane ] )
                            );
        phi[ijk] = phi[ijk] - lambda[ijk]*(helmholtz-rhs[ijk]);
      }}
      #else
      for(j=1-ghosts;j<DimJ+ghosts-1;j++){
      for(i=0-ghostsToOperateOn;i<DimI+ghostsToOperateOn;i++){
      if((i^j^k^planeInWavefront^sweep^1)&1){ // looks very clean when [0] is i,j,k=0,0,0 
        int ijk = i + j*pencil + k*plane;
        double helmholtz =  a*alpha[ijk]*phi[ijk]
                           -b*h2inv*(
                              beta_i[ijk+1     ]*( phi[ijk+1     ]-phi[ijk       ] )
                             -beta_i[ijk       ]*( phi[ijk       ]-phi[ijk-1     ] )
                             +beta_j[ijk+pencil]*( phi[ijk+pencil]-phi[ijk       ] )
                             -beta_j[ijk       ]*( phi[ijk       ]-phi[ijk-pencil] )
                             +beta_k[ijk+plane ]*( phi[ijk+plane ]-phi[ijk       ] )
                             -beta_k[ijk       ]*( phi[ijk       ]-phi[ijk-plane ] )
                            );
        phi[ijk] = phi[ijk] - lambda[ijk]*(helmholtz-rhs[ijk]);
      }}}
      #endif
    }} // wavefront
    #pragma omp barrier
  } // leadingK
  } // omp parallel
}

//------------------------------------------------------------------------------------------------------------------------------
void __box_smooth_GSRB_multiple(box_type *box, int phi_id, int rhs_id, double a, double b, int sweep){
  int pencil = box->pencil;
  int  plane = box->plane;
  int ghosts = box->ghosts;
  int   DimI = box->dim.i;
  int   DimJ = box->dim.j;
  int   DimK = box->dim.k;
  double h2inv = 1.0/(box->h*box->h);
  double  * __restrict__ phi          = box->grids[  phi_id] + ghosts*(1+pencil+plane);
  double  * __restrict__ rhs          = box->grids[  rhs_id] + ghosts*(1+pencil+plane);
  double  * __restrict__ alpha        = box->grids[__alpha ] + ghosts*(1+pencil+plane);
  double  * __restrict__ beta_i       = box->grids[__beta_i] + ghosts*(1+pencil+plane);
  double  * __restrict__ beta_j       = box->grids[__beta_j] + ghosts*(1+pencil+plane);
  double  * __restrict__ beta_k       = box->grids[__beta_k] + ghosts*(1+pencil+plane);
  double  * __restrict__ lambda       = box->grids[__lambda] + ghosts*(1+pencil+plane);

  int leadingK,planeInWavefront;
  int kLow  =     -(ghosts-1);
  int kHigh = DimK+(ghosts-1);
  for(leadingK=kLow;leadingK<kHigh;leadingK++){
    for(planeInWavefront=0;planeInWavefront<ghosts;planeInWavefront++){
    int k=(leadingK-planeInWavefront);if((k>=kLow)&&(k<kHigh)){
      int i,j;
      int ghostsToOperateOn=ghosts-1-planeInWavefront;
      #if   defined(__GSRB_CONDITIONAL)
      for(j=0-ghostsToOperateOn;j<DimJ+ghostsToOperateOn;j++){
      for(i=0-ghostsToOperateOn;i<DimI+ghostsToOperateOn;i++){
        int ijk = i + j*pencil + k*plane;
        int doit = ((i^j^k^planeInWavefront^sweep^1)&1);
        double helmholtz =  a*alpha[ijk]*phi[ijk]
                           -b*h2inv*(
                              beta_i[ijk+1     ]*( phi[ijk+1     ]-phi[ijk       ] )
                             -beta_i[ijk       ]*( phi[ijk       ]-phi[ijk-1     ] )
                             +beta_j[ijk+pencil]*( phi[ijk+pencil]-phi[ijk       ] )
                             -beta_j[ijk       ]*( phi[ijk       ]-phi[ijk-pencil] )
                             +beta_k[ijk+plane ]*( phi[ijk+plane ]-phi[ijk       ] )
                             -beta_k[ijk       ]*( phi[ijk       ]-phi[ijk-plane ] )
                            );
        phi[ijk] = (doit) ? phi[ijk] - lambda[ijk]*(helmholtz-rhs[ijk]) : phi[ijk];
      }}
      #elif defined(__GSRB_STRIDE2)
      for(j=0-ghostsToOperateOn;j<DimJ+ghostsToOperateOn;j++){
      for(i=((j^k^sweep)&1)-ghostsToOperateOn;i<DimI+ghostsToOperateOn;i+=2){ // stride-2 GSRB
        int ijk = i + j*pencil + k*plane;
        double helmholtz =  a*alpha[ijk]*phi[ijk]
                           -b*h2inv*(
                              beta_i[ijk+1     ]*( phi[ijk+1     ]-phi[ijk       ] )
                             -beta_i[ijk       ]*( phi[ijk       ]-phi[ijk-1     ] )
                             +beta_j[ijk+pencil]*( phi[ijk+pencil]-phi[ijk       ] )
                             -beta_j[ijk       ]*( phi[ijk       ]-phi[ijk-pencil] )
                             +beta_k[ijk+plane ]*( phi[ijk+plane ]-phi[ijk       ] )
                             -beta_k[ijk       ]*( phi[ijk       ]-phi[ijk-plane ] )
                            );
        phi[ijk] = phi[ijk] - lambda[ijk]*(helmholtz-rhs[ijk]);
      }}
      #else
      for(j=0-ghostsToOperateOn;j<DimJ+ghostsToOperateOn;j++){
      for(i=0-ghostsToOperateOn;i<DimI+ghostsToOperateOn;i++){
      if((i^j^k^planeInWavefront^sweep^1)&1){ // looks very clean when [0] is i,j,k=0,0,0 
        int ijk = i + j*pencil + k*plane;
        double helmholtz =  a*alpha[ijk]*phi[ijk]
                           -b*h2inv*(
                              beta_i[ijk+1     ]*( phi[ijk+1     ]-phi[ijk       ] )
                             -beta_i[ijk       ]*( phi[ijk       ]-phi[ijk-1     ] )
                             +beta_j[ijk+pencil]*( phi[ijk+pencil]-phi[ijk       ] )
                             -beta_j[ijk       ]*( phi[ijk       ]-phi[ijk-pencil] )
                             +beta_k[ijk+plane ]*( phi[ijk+plane ]-phi[ijk       ] )
                             -beta_k[ijk       ]*( phi[ijk       ]-phi[ijk-plane ] )
                            );
        phi[ijk] = phi[ijk] - lambda[ijk]*(helmholtz-rhs[ijk]);
      }}}
      #endif
    }} // wavefront
  } // leadingK
}


//==================================================================================================
void smooth(domain_type * domain, int level, int phi_id, int rhs_id, double a, double b, int sweep){
  uint64_t _timeStart = CycleTime();
  int CollaborativeThreadingBoxSize = 100000; // i.e. never
  #ifdef __COLLABORATIVE_THREADING
    CollaborativeThreadingBoxSize = 1 << __COLLABORATIVE_THREADING;
  #endif
  int box;
//  if(domain->subdomains[0].levels[level].dim.i >= CollaborativeThreadingBoxSize){
//    for(box=0;box<domain->subdomains_per_rank;box++){__box_smooth_GSRB_multiple_threaded(&domain->subdomains[box].levels[level],phi_id,rhs_id,a,b,sweep);}
//  }else{
    #pragma omp parallel for private(box)
    for(box=0;box<domain->subdomains_per_rank;box++){__box_smooth_GSRB_multiple(&domain->subdomains[box].levels[level],phi_id,rhs_id,a,b,sweep);}
//  }
  domain->cycles.smooth[level] += (uint64_t)(CycleTime()-_timeStart);
}
//==================================================================================================
